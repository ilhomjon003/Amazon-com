const CUSTOMER_DATA = {
  Recommended: [
    {
      title: "Find a missing package that shows as 'Delivered' ",
      desc: "Most packages arrive on time, but, sometimes, the tracking may show as 'delivered' and you don't have your package.",
    },
    {
      title: "late deliveries",
      desc: "Most packages arrive on time. Orders sometimes show up after the estimated delivery date.",
    },
    {
      title: "track your package",
      desc: "You can find tracking information in your order details. If an order includes multiple items, each may have separate delivery dates and tracking information...",
    },
    {
      title: "check status of a refund",
      desc: "You can check the status of your refund in Your Orders .",
    },
    {
      title: "find a missing item from your package",
      desc: "If an item is missing from your package, it may have been shipped separately.",
    },
    {
      title: "check status of refund",
      desc: "You can check the status of your refund in Your Orders .",
    },
    {
      title: "replace an item",
      desc: "If you received a damaged, defective, or incorrect item sold by Amazon, you can request a replacement for a small number of eligible items through Your Orders...",
    },
    {
      title: "cancel items or ordes",
      desc: "You can cancel items or orders that haven't entered the shipping process yet.",
    },
  ],
  Stuff: [
    {
      title: "Track your package",
      desc: "You can find tracking information in your order details. If an order includes multiple items, each may have separate delivery dates and tracking information...",
    },
    {
      title: "Find a missing package that shows as 'Delivered' ",
      desc: "Most packages arrive on time, but, sometimes, the tracking may show as 'delivered' and you don't have your package.",
    },
    {
      title: "Undeliverable Packages",
      desc: "Occasionally, packages are returned to us as undeliverable.",
    },
    {
      title: "Find a missing package from your package",
      desc: "If an item is missing from your package, it may have been shipped separately.",
    },
    {
      title: "More in where's my Stuff",
    },
  ],
  Shipping: [
    {
      title: "Carries Contact info",
      desc: "We work with the following carriers to deliver items. If you have an issue with your delivery, you can contact the carriers directly.",
    },
    {
      title: "Delivery Guarantiees",
      desc: "We offer guaranteed delivery on certain delivery speeds and select products. When guaranteed delivery is available on an order, we'll state this on the checkout page, with the associated delivery date and cost..",
    },
    {
      title: "Large Items and Heavy-Bulky Services",
    },
    {
      title: "guaranteed delivery term and Conditions",
      desc: "If we provide a guaranteed delivery date on the checkout page, your shipping fees may be refunded if we miss our promised delivery date.",
    },
    {
      title: "collection points",
    },
    {
      title: "Amazon prime benefits",
    },
    {
      title: "Amazon prime and term conditions",
      desc: "",
    },
    {
      title: "end your amazon prime membership",
      desc: "You can end your Prime membership by selecting the End Membership button on this page.",
    },
    {
      title: "Amazon prime membership fee ",
      desc: "When your free trial or membership period ends, we'll automatically charge for the next membership period.",
    },
    {
      title: "more information in shipping and delivery",
      desc: "",
    },
  ],
  Returns: [
    {
      title: "about our return policies",
      desc: "mazon.com and most sellers on Amazon.com offer returns for items within 30 days of receipt of shipment.",
    },
    {
      title: "you items you ordered",
      desc: "You can return many eligible items sold on Amazon. When you return an item, you may see different return options depending on the seller, item, or reason for return.",
    },
    {
      title: "return a gift",
      desc: "The Returns Center allows gift recipients to return items marked as a gift at the time of purchase.",
    },
    {
      title: "return costs",
      desc: "Your return shipment is free of charge in some cases. If you return an item using the return label provided in the Returns Center and the reason for return isn't a result of an Amazon.com error, the cost of return shipping will be",
    },
    {
      title: "track your return",
      desc: "You can stay on top of your returns by tracking them in Your Orders..",
    },
    {
      title: "refunds",
      desc: "When you return an item, your refund amount and refund method may vary. Check the payment method that was refunded and the status of your refund in Your Orders.",
    },
    {
      title: "check the status of your refund",
      desc: "You can check the status of your refund in Your Orders.",
    },
    {
      title: "replace an item",
      desc: "If you received a damaged, defective, or incorrect item sold by Amazon, you can request a replacement for a small number of eligible items through Your Orders.",
    },
    {
      title: "exchanges and replacements",
      desc: "A small number of eligible items can be exchanged or replaced through Your Orders if your exchange or replacement meets certain criteria, including stock availability.",
    },
    {
      title: "More in Returns and Refunds",
    },
  ],

  Account: [
    {
      title: "Find a missing package that shows as 'Delivered' ",
      desc: "Most packages arrive on time, but, sometimes, the tracking may show as 'delivered' and you don't have your package.",
    },
    {
      title: "late deliveries",
      desc: "Most packages arrive on time. Orders sometimes show up after the estimated delivery date.",
    },
    {
      title: "track your package",
      desc: "You can find tracking information in your order details. If an order includes multiple items, each may have separate delivery dates and tracking information...",
    },
    {
      title: "check status of a refund",
      desc: "You can check the status of your refund in Your Orders .",
    },
    {
      title: "find a missing item from your package",
      desc: "If an item is missing from your package, it may have been shipped separately.",
    },
    {
      title: "check status of refund",
      desc: "You can check the status of your refund in Your Orders .",
    },
    {
      title: "replace an item",
      desc: "If you received a damaged, defective, or incorrect item sold by Amazon, you can request a replacement for a small number of eligible items through Your Orders...",
    },
    {
      title: "more in managing your account",
    },
  ],

  Security: [
    {
      title: "track your package",
      desc: "You can find tracking information in your order details. If an order includes multiple items, each may have separate delivery dates and tracking information...",
    },
    {
      title: "check status of a refund",
      desc: "You can check the status of your refund in Your Orders .",
    },
    {
      title: "find a missing item from your package",
      desc: "If an item is missing from your package, it may have been shipped separately.",
    },
    {
      title: "check status of refund",
      desc: "You can check the status of your refund in Your Orders .",
    },
    {
      title: "replace an item",
      desc: "If you received a damaged, defective, or incorrect item sold by Amazon, you can request a replacement for a small number of eligible items through Your Orders...",
    },
    {
      title: "cancel items or ordes",
      desc: "You can cancel items or orders that haven't entered the shipping process yet.",
    },
  ],
  Payment: [
    {
      title: "Delivery Guarantiees",
      desc: "We offer guaranteed delivery on certain delivery speeds and select products. When guaranteed delivery is available on an order, we'll state this on the checkout page, with the associated delivery date and cost..",
    },
    {
      title: "Large Items and Heavy-Bulky Services",
    },
    {
      title: "Unknown Charges",
      desc: "There are several reasons why you might not recognize a charge",
    },
    {
      title: "Amazon prime benefits",
    },
    {
      title: "Amazon prime and term conditions",
    },
    {
      title: "end your amazon prime membership",
      desc: "You can end your Prime membership by selecting the End Membership button on this page.",
    },
    {
      title: "More in Payments, Pricing and Promotions",
    },
  ],
  Device: [
    {
      title: "Carries Contact info",
      desc: "We work with the following carriers to deliver items. If you have an issue with your delivery, you can contact the carriers directly.",
    },
    {
      title: "Delivery Guarantiees",
      desc: "We offer guaranteed delivery on certain delivery speeds and select products. When guaranteed delivery is available on an order, we'll state this on the checkout page, with the associated delivery date and cost..",
    },
    {
      title: "Large Items and Heavy-Bulky Services",
    },
    {
      title: "guaranteed delivery term and Conditions",
      desc: "If we provide a guaranteed delivery date on the checkout page, your shipping fees may be refunded if we miss our promised delivery date.",
    },
    {
      title: "collection points",
    },
    {
      title: "Amazon prime membership fee ",
      desc: "When your free trial or membership period ends, we'll automatically charge for the next membership period.",
    },
    {
      title: "Ask the digital & Device community",
    },
  ],
  Business: [
    {
      title: "Amazon Business Help",
    },
    {
      title: "using amazon prime with your business account",
      desc: "Business Prime is an annual membership program that provides business-specific features and benefits. These include unlimited fast delivery on eligible items, advanced visibility of your organization's spend, and more control over purchasing across your organization.",
    },
    {
      title: "Tax Exemption",
      desc: "Amazon Business provides purchasing solutions that allow registered businesses and their employees to shop for business supplies on Amazon. You can customize your business account to suit your business needs.",
    },
    {
      title: "Term & conditions",
      desc: "These Amazon Business Accounts Terms & Conditions are between Amazon.com Services LLC and the entity registering for a Business Account.",
    },
    {
      title: "trademark guidlines",
      desc: "These Guidelines apply to your use of trademarks owned by Amazon.com . You may only use the specific trademarks identified by Amazon materials that have been approved..",
    },
  ],
  Items: [
    {
      title: "Track your package",
      desc: "You can find tracking information in your order details. If an order includes multiple items, each may have separate delivery dates and tracking information...",
    },
    {
      title: "Find a missing package that shows as 'Delivered' ",
      desc: "Most packages arrive on time, but, sometimes, the tracking may show as 'delivered' and you don't have your package.",
    },
    {
      title: "Undeliverable Packages",
      desc: "Occasionally, packages are returned to us as undeliverable.",
    },
    {
      title: "Find a missing package from your package",
      desc: "If an item is missing from your package, it may have been shipped separately.",
    },
    {
      title: "More in Large items and Heavy-bulky services",
    },
  ],
  Others: [
    {
      title: "Gifts, Gift Cards, and Registries",
      desc: "When you redeem an Amazon.com Gift Card or gift voucher to your account, the funds are stored in Your Account and will automatically apply to your next eligible order",
    },
    {
      title: "Redeem a Gift Card",
    },
    {
      title: "Site Features"
    },
    {
      title: "Ordering",
    },
    {
      title: "A-to-z Guarantee"
    },
    {
      title: "Publisher & Vendor Help"
    },
    {
      title: "Amazon Games"
    },
    {
      title: "Amazon Physical Stores"
    },
    {
      title: "Amazon Pharmacy"
    },
    {
      title: "Help for Amazon Sellers"
    },
    {
      title: "Associates Program Help"
    },
    {
      title: "Amazon Web Services"
    }
  ],
};

export default CUSTOMER_DATA;
